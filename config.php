<?php

/**
 * Configuration for database connection
 *
 */

$host       = getenv("DBHOST");
$username   = getenv("DBUSER");
$password   = getenv("DBPASS");
$dbname     = "phpapp";
$dsn        = "mysql:host=$host;dbname=$dbname";
$options    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );
